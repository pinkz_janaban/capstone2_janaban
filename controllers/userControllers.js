const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
		})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			console.log(user);
			return true;
		}
	})
	
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {

		//User does not exist
		if(result == null){
			return false;
		}
		//User exists
		else{
			//Syntax: bcrypt.compareSync(data, encrypted);
			//reqBody.password & result.password
			//bcrypt.compareSync() return Boolean
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			//If the passwords match the result.
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}

			}
			//Password do not match
			else{
				return false;
			}
		}
	})
}

module.exports.editProfile = (userId, reqBody) => {
	
	const updateUser = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: reqBody.password,
		mobileNo: reqBody.mobileNo,
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(userId, updateUser, {new: true}).then((result, error) => {
		//console.log(result)
		if(error){
			return error
		} else {
			return result
		}
	})
}



module.exports.getProfile = (data) => {
	// console.log(data)
	const {id} = data

	return User.findById(id).then((result, err) => {
		// console.log(result)

		if(result != null){
			result.password = "";
			return result
		} else {
			return false
		}
	})
}

