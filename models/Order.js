const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	user: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: "User"
	},
	orderItems: [
		{
			product: {
				type: mongoose.Schema.Types.ObjectId,
				required: true,
				ref: "Product"
			},
			quantity: {
				type: Number,
				default: 1
			},
			price: {
				type: Number,
				required: true
			}

		}
	],
	shippingAddress: {
		address: {
			type: String,
			required: true
		},
		city: {
			type: String,
			required: true
		},
		postalCode: {
			type: String,
			required: true
		},
		country: {
			type: String,
			required: true
		}
	},
	createdOn:{
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);