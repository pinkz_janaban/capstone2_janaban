const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");

const auth = require("./../auth");

//Create product
router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
   
    if(userData.isAdmin) {
        productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
    }
    else{
        res.send("You don't have permission on this page!");
    }
})

//Get all active products
router.get("/all", auth.verify, (req, res) => {
    productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
})

//Get specific product
router.get("/:productId", auth.verify, (req, res) => {

    let paramsId = req.params.productId
    productControllers.getProductById(paramsId).then(resultFromController => res.send(resultFromController))
})

//update product info
router.put("/:productId", auth.verify, (req, res) => {

    productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})

//archive product
router.put("/:productId/archive", auth.verify, (req, res) => {
    productControllers.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})



module.exports = router;