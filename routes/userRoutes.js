const express = require("express");
const router = express.Router();

const User = require("../models/User");

const userControllers = require("../controllers/userControllers")

const auth = require("../auth");


//Router for the user registration
router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for the user authentication
router.post("/login", (req, res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


//Route for updating to admin
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {
	let userId = auth.decode(req.headers.authorization).id

	userControllers.editProfile(userId, req.body).then(resultFromController => res.send(resultFromController))
})


//Route for the retrieving user information
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization); //contains the token
	console.log(userData);

	//Provides the user's ID for the getProfile controller method
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

module.exports = router;